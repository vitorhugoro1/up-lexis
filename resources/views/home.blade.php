@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <a href="{{ route('sintegra.search') }}" class="btn btn-primary">Pesquisar no Sintegra</a>
                    <a href="{{ route('sintegra.index') }}" class="btn btn-primary">Pesquisas feitas</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
