@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Pesquisas feitas no Sintegra</div>

                    <div class="card-body table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th> CNPJ </th>
                                    <th> Resultado </th>
                                    <th> Data da Pesquisa </th>
                                    <th> Ação </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($sintegra as $enterprise)
                                    <tr>
                                        <td>{{ $enterprise->cnpj }}</td>
                                        <td>
                                            <sintegra-modal-result
                                                    :sintegra="{{ json_encode($enterprise->data) }}"
                                                    :id="{{ $enterprise->id }}"
                                            ></sintegra-modal-result>
                                        </td>
                                        <td>{{ $enterprise->created_at->format('d/m/Y') }}</td>
                                        <td>
                                            <button type="button" class="btn btn-outline-danger" onclick="event.preventDefault(); document.getElementById('delete-{{ $enterprise->id }}').submit();">
                                                Deletar
                                            </button>
                                            <form id="delete-{{ $enterprise->id }}" action="{{ route('sintegra.destroy', $enterprise->id) }}" method="POST" style="display: none;">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                            </form>
                                        </td>
                                    </tr>
                                    @empty
                                        <tr>
                                            <td colspan="4">
                                                Sem Pesquisas
                                            </td>
                                        </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{ $sintegra->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection