@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Realizar uma pesquisa no Sintegra</div>

                    <div class="card-body">
                        <sintegra-search></sintegra-search>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection