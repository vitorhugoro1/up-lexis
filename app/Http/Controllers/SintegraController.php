<?php

namespace App\Http\Controllers;

use App\Sintegra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SintegraController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$sintegra = Sintegra::whereHas('user', function ($query) {
			$query->where('id', Auth::user()->id);
		})->latest()->paginate(5);

		return view('sintegra.index')->with(compact('sintegra'));
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        return view('sintegra.search');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sintegra  $sintegra
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sintegra $sintegra)
    {
	    $sintegra->delete();

        return redirect()->route('sintegra.index');
    }
}
