<?php

namespace App\Http\Controllers\Admin;

use App\Services\SintegraService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SintegraController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 * @param $cnpj
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function search(Request $request, $cnpj)
    {
	    $error = $this->validateLogin($request);

    	if ( !$error ) {
    		return response()->json([
    			'data'  => [
    				'message'   => 'username or password wrong. try again'
			    ]
		    ]);
	    }

	    $crawler = app(SintegraService::class)->searchInSintegra($cnpj);

        return response()->json([
        	'data'  => $crawler
        ]);
    }

	/**
	 * @param Request $request
	 *
	 * @return bool
	 */
    private function validateLogin (Request $request)
    {
//	    if (Auth::check()) {
//	    	return true;
//	    }
//
//	    $validator = Validator::make($request->all(), [
//		    'username' => 'required|email',
//		    'password' => 'required|alphaNum|min:3',
//	    ]);
//
//	    if ( $validator->fails() ) {
//	    	return false;
//	    }

//	    if ( Auth::attempt(['email' => $request->username, 'password' => $request->password]) ) {
//	    	return true;
//	    }

	    return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

}
