<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property array $data
 */
class Sintegra extends Model
{
	use SoftDeletes;

    protected $table = 'sintegra';

    protected $fillable = ['user_id', 'cnpj', 'data'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $casts = [
    	'data'  => 'array'
    ];

    public function user ()
    {
    	return $this->belongsTo(User::class);
    }

    public function toArray() {
	    return [
	    	'id'            => $this->id,
	    	'data'          => $this->data,
		    'created_at'    => $this->created_at->toAtomString()
	    ];
    }
}
