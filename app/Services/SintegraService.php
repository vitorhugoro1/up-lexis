<?php


namespace App\Services;


use App\Sintegra;
use Goutte\Client;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\DomCrawler\Crawler;

class SintegraService
{
	private $url = "http://www.sintegra.es.gov.br";

	private $assertUri = "http://www.sintegra.es.gov.br/resultado.php";

	/**
	 * @var Client
	 */
	private $client;

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	/**
	 * @param $cnpj
	 *
	 * @return array
	 */
	public function searchInSintegra( $cnpj )
	{
		$crawler = $this->client->request('GET', $this->url);
		$form = $crawler->selectButton('Consultar')->form();

		$result = $this->client->submit($form, ['num_cnpj' => $cnpj]);

		if ( $result->getUri() !== $this->assertUri ) {
			$message = $result->filter('div.erro')->first();

			return [
				'errors' => [
					'message'   => trim($message->text(), "- ")
				]
			];
		}

		$result = $this->catchResult($result);

		$sintegra = [
			'user_id'   => Auth::user()->id,
			'cnpj'      => $cnpj,
			'data'      => $result->toArray()
		];

		return Sintegra::create($sintegra);
	}

	/**
	 * @param Crawler $crawler
	 *
	 * @return static|Collection
	 */
	private function catchResult(Crawler $crawler)
	{
		$sintegra = collect([]);

		$principalTable = $crawler->filter('table.resultado')->first();

		$childTables = $principalTable->children()->filter('table');

		$childTables->each( function (Crawler $node) use (&$sintegra) {
			$nodes = $node->filter('tr')->reduce(function (Crawler $node) {
					return $node->filter('td.secao')->count() === 0;
				});

			$preview = null;

			$nodes->filter('td')->each(function (Crawler $row) use (&$preview, &$sintegra) {
				if ($row->attr('class') === 'titulo') {
					$preview = str_slug($row->text(), '_');
					$sintegra[ $preview ] = '';
				}

				if($row->attr('class') === 'valor') {
					$sintegra[$preview] = trim($row->text());
				}
			});
		});

		return $sintegra->reject(function ($item, $key) {
			return $key === 0;
		});
	}
}