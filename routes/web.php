<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('sintegra')->name('sintegra.')->group(function () {
	Route::get('/search', [ 'uses' => 'SintegraController@search' ])->name('search');
	Route::get('/', ['uses' => 'SintegraController@index' ])->name('index');
	Route::delete('/{sintegra}', ['uses' => 'SintegraController@destroy'])->name('destroy');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('api/sintegra/es/{cnpj}', ['uses' => 'Admin\SintegraController@search']);
