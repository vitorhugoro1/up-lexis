<?php

use Faker\Generator as Faker;

$factory->define(App\Sintegra::class, function (Faker $faker) {
    return [
        'user_id'   => 1,
	    'cnpj'      => 31669138588095,
	    'data'      => json_encode($faker->paragraphs(3, false), 128)
    ];
});
