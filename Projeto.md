# Rotas

GET /api/sintegra/es/{cnpj}/
GET /sintegra/es/search
GET /sintegra

# Acessos padrões

- email: user@example.com
- senha: secret

# Comandos Iniciais

composer install
composer run-script post-root-package-install
composer run-script post-create-project-cmd
composer dump-autoload

php artisan migrate
php artisan db:seed

npm install